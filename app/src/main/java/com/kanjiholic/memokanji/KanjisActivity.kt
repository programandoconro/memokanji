package com.kanjiholic.memokanji

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

class KanjisActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kanjis)

        val nextButton: Button = findViewById(R.id.next_button)
        val addKanjiButton: FloatingActionButton = findViewById(R.id.go_to_add_kanji)
        val tx: TextView = findViewById(R.id.kanji)
        val intent = Intent(this, AddKanjiActivity::class.java)

        nextButton.setOnClickListener{
            Snackbar.make(tx, nextButton.text, Snackbar.LENGTH_SHORT)
                .show()
            if (nextButton.text !== "SHOW") {
                nextButton.text = "SHOW"
            }
            else{
                nextButton.text = "NEXT"
            }
        }

        addKanjiButton.setOnClickListener{
            startActivity(intent)
        }


    }


}